


import * as arsdk from '../arsdk/arsdk'
import { ECSComponent } from '../arsdk/narrow-intf/arsdk-impl'
let print = arsdk.print


//  

class OnFrameSystem extends arsdk.ECSOnFrame { 
    constructor() { 
        super(
            arsdk.ecsExperience, 
            () => { this.onFrame() })
        arsdk.ecsExperience.addComponent(this)
    }
    addEntity(entity: arsdk.ECSEntity) { 
        this.entities.push(entity)
    }

    protected entities: arsdk.ECSEntity[] = []
    protected onFrame() {
        for (let entity of this.entities) { 
            let onFrameComponent: OnFrameComponent = entity.getComponent(OnFrameComponent)
            
        }
    }
}

class OnFrameComponent extends arsdk.ECSEmptyComponent { 
    constructor(ownerEntity: arsdk.ECSEntity) { 
        super(ownerEntity)
    }
    onFrame() { 
    }
}



class RigidBody extends arsdk.ECSEmptyComponent { 
    velocity: number[] = [0, 0, 0]
    initialYPos?: number
}

class PhysicsSystem extends OnFrameSystem { 
    protected onFrame() { 
        // componentsByEntities: ECSComponent[][] = 
        //     arsdk.experience.getComponents(this.entities, [OnFrameComponent, arsdk.ECSTransform])
        for (let entity of this.entities) { 
            const rigidBody = entity.getComponent(RigidBody)
            const transform = entity.getComponent(arsdk.ECSTransform)

            let worldPos = transform.getWorldPosition()
            if (rigidBody.initialYPos === undefined)
                rigidBody.initialYPos = worldPos[1]
            worldPos[0] += rigidBody.velocity[0]
            worldPos[1] += rigidBody.velocity[1]
            worldPos[2] += rigidBody.velocity[2]
            rigidBody.velocity[1] -= 0.01

            if (worldPos[1] < rigidBody.initialYPos) { 
                rigidBody.velocity[1] = 0
                worldPos[1] = rigidBody.initialYPos
            }

            if (worldPos[1] == rigidBody.initialYPos) { 
                rigidBody.velocity[0] *= 0.7
                rigidBody.velocity[1] *= 0.7
                rigidBody.velocity[2] *= 0.7
            }

            transform.setWorldPosition(worldPos)
        }
    }
}



try {
 
    let physicsSystem = new PhysicsSystem()
    
    arsdk.experience.addObjectScript(
        "cube_scene", 
        (object: arsdk.ECSEntity) => { 

            let rigidBodyEntities: arsdk.ECSEntity[] = []

            //  cubes RigidBody
            const cubesParentTransform = object.getComponent(arsdk.ECSTransform)?.getChild(1)
            if (cubesParentTransform === null) 
                return
            const cubeCount = Math.min(16, cubesParentTransform.getChildCount())
            for (let i = 0; i < cubeCount; i++) {
                let cubeEntity = cubesParentTransform.getChild(i)?.getEntity()
                if (cubeEntity == null) 
                    continue
                physicsSystem.addEntity(cubeEntity)
                rigidBodyEntities.push(cubeEntity)
                let rigidBody = new RigidBody(cubeEntity)
                cubeEntity.addComponent(rigidBody)
        
                //set cube to random color
                // rigidBodyEntities[i].getComponent(MeshRenderable).material.setColor(experience.random(), experience.random(), experience.random(), experience.random());
            }

            //  sphere RigidBody + onTap
            let sphereEntity = object.getComponent(arsdk.ECSTransform)?.
                getChild(0)?.getChild(2)?.getEntity()
            if (sphereEntity == null) { 
                print("sphereEntity == null")
                return
            }
            let sphereRigidBody = new RigidBody(sphereEntity)
            sphereEntity.addComponent(sphereRigidBody)
            physicsSystem.addEntity(sphereEntity)
            rigidBodyEntities.push(sphereEntity)
            let sphereOnTap = new arsdk.ECSOnTap(
                sphereEntity, 
                (tapWorldPosition) => { 
                    sphereRigidBody.velocity = [0, 0.3, 0]
                })

            //  plane onTap
            let planeEntity = object.getComponent(arsdk.ECSTransform)?.
                getChild(0)?.getChild(1)?.getEntity()
            if (planeEntity == null) {
                print("planeEntity == null")
                return
            }
            let planeOnTap = new arsdk.ECSOnTap(
                planeEntity, 
                (tapWorldPosition) => { 
                    for (let i = 0; i < rigidBodyEntities.length; ++i) {
                        let rigidBodyEntity = rigidBodyEntities[i]
                        let rigidBodyComponent = rigidBodyEntity.getComponent(RigidBody)
                        if (rigidBodyComponent == null)
                            continue
                        let rigidBodyTransform = rigidBodyEntity.getComponent(arsdk.ECSTransform)
                        if (rigidBodyTransform == null) 
                            continue
                        let rigidBodyWorldPos = rigidBodyTransform.getWorldPosition()
                        
                        let dir = rigidBodyWorldPos
                        dir[1] += 0.1
                        dir[0] -= tapWorldPosition[0]
                        dir[1] -= tapWorldPosition[1]
                        dir[2] -= tapWorldPosition[2]
                        let dist = Math.sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2])

                        if (dist < 0.5) { 
                            //  dir.normalize()
                            dir[0] /= dist
                            dir[1] /= dist
                            dir[2] /= dist
                            //  dir.scale(0.1)
                            dir[0] *= 0.1
                            dir[1] *= 0.1
                            dir[2] *= 0.1
                            rigidBodyComponent.velocity = dir
                        }
                    }
                })
            planeEntity.addComponent(planeOnTap)
            
        })
} catch(x) {
    print("exception:")
    print(x)
}
