
Implementation notes

todo write doc about
*   android's wide interface vs narrow interface on ios / remote
*   bootstrap sequence in the wide / narrow intf cases. 
    *   what's the entry point file (not main, but xyz-bootstrap)
    *   how does main get executed
    *   what happens on experience restart
*   publishing, parcel bundle
    *   vs android (how it ignores the narrow-intf parts still present)
*   native -> script callbacks
    *   wide: not impl (handled by rhino)
    *   narrow: 
        *   narrow-inmtf support code provides some helpers, 
            but ultimately implemented on the native side. 
        *   ios: arsdk_executeCallback()
        *   remote: 
*   remote implementation
    *   threading model, blocking, worker thread, data sharing

----

    Narrow Interface vs Wide Interface

Parts of the code and comments mention platforms using a "narrow interface" or 
a "wide interface". This refers to the interface between the native SDK and the script side. 

Wide interface: 
*   All ARSDK symbols (classes, global variables) are exposed (injected into the 
    JavaScript context) by the native-side SDK. 
*   The prime example for a wide-interface platform is Android (using Rhino). 

Narrow interface: 
*   The native side only exposes a small interface similar to a 
    simple generic, JSON-string-based Remote Procedure Call interface. 
*   The most important element of that interface is an executeNative_str() function. 
    *   Implements a JSON-string-based Remote Procedure Call. 
    *   Takes a string that encodes a JSON object that describes a function call. 
    *   Returns a string that encodes a JSON object that describes the result of 
        executing that function call on the native side. 
*   The ARSDK scripting interface (ARSDK classes etc.) is then implemented 
    on the script side, on top of the narrow interface. 
    *   For example, consider an `ARObject` JavaScript class with a setOpacity() function; 
        the body of that function builds an RPC JSON and calls executeNative_str(). 
    *   See narrow-intf/arsdk-impl.ts
*   Bootstrapping is a bit more complicated on narrow-interface platforms, 
    because the native side may need to do be able to inject some initialization 
    when the narrow-intf support code is already loaded, but the user script 
    hasn't started yet. 
    See Bootstrapping below. 
*   iOS and remote scripting use a narrow interface. 
    *   On iOS the narrow interface's implementation (executeNative_str()) 
        is exposed directly by the native SDK. 
    *   When remote scripting, the narrow interface is implemented on top of some 
        helper classes that can send the RPC string over network to the native app. 
*   Note the narrow-intf support code (that implements ARSDK classes using 
    executeNative_str()) is also present in the script bundle when running on Android, 
    but is ignored. See Bundling below. 


    Bootstrapping

When executing an experience script, the entry point is _not_ 
the main user script (main.ts), but rather a bootstrapper (see xyz-bootstrap.ts). 
main.ts is loaded later as a module. 
*   When bundling for on-device execution, the entry point is device-bootstrap.ts, 
    main.ts is bundled as a module. 
    When executing the bundle, the code in bootstrap.ts runs and loads main.ts. 
*   When cloud scripting, launch.json is set up to execute remote-bootstrap.ts, 
    and again, the bootstrapper loads main.ts as a module. 

On wide-interface platforms (Android), the bootstrapper simply loads 
and executes the main user script. 

On narrow-interface platforms the bootstrapping sequence is more complicated. 
*   The reason is that narrow-intf platforms may need to be able to do some native-side 
    initialization after the script bundle (including the narrow-intf support code) 
    is already loaded, but before executing the user script. 
*   The most important example is exporting global objects. 
    *   Exporting a global `x` of type `X` is something like `var x = new X()` 
        on the script side. 
    *   For this to work, we need to know the `X` class. 
    *   On narrow-intf platforms, ARSDK classes (`X` in this example) are defined 
        on the script side (see narrow-intf/arsdk-impl.ts). 
    *   This means that such declarations only work _after_ loading the script bundle. 
    *   But we can only execute the user script _after_ globals are already available. 
    *   So, we need to first load the script bundle, then the native side can export globals, 
        and only then can we execute the user script. 
    *   (On wide-intf platforms both classes and globals are exposed by the native side, 
        so the native side can easily make sure that classes are exposed first.)

iOS: 
*   todo

Remote: 
*   todo

    Bundling / Publishing

    Remote Scripting