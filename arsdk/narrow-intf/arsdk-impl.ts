
/*
    Implements the ARSDK interface on top of a narrow interface, 
    defined by Requirements. 
    You pass in a suitable implementation of Requirements, 
    you get back working implementations of ARSDK classes and functions
    (the same ones defined in arsdk.d.ts). 
    Used on iOS and in cloud scripting. 
    For Requirements, you most likely will want to use an instance of ARScript
    (and ARScript will need an implementation of executeNative_str). 
    Usage example: 
        import * as arsdk from "../narrow-intf/arsdk-impl"
        import { ARScript } from "../arscript"
        let arscript = new ARScript()
        arsdk.init(arscript)
    See remote-bootstrap.ts for a usage example. 

    Note in some cases there might be typing differences between arsdk.d.ts 
    and the implementations here. One example is constructors: arsdk.d.ts only 
    defines the "real" constructor of a class, the one that could be called 
    from the user script, while the implementation here also allows special 
    constructor calls, eg. when replicating an object created on the native side. 
    TODO do we even need to type the special constructors here? 
        we just forward all ctor params to replicateToNative anyway, 
        and replicateToNative recognizes special ctor calls by the 
        dynamic type or value of the first param. 
        so maybe here we could just define ctors to get a list of `any` params 
        (just like how replicateToNative takes a list of `any`s anyway)? 
        if we keep the typed ctors, do we get any extra runtime checks? 
        (we don't get any compile checks, we get compile checks from what's in arsdk.d.ts.)
    TODO is there even a point in this file being TS not JS? 
        pretty much all functions here just forward to something that 
        takes all parameters as `any`. 
        at the call site, type checking is done based on arsdk.d.ts, not this file. 
        this is loaded dynamically by the bootstrapper, 
        and then the symbols are type-blindly paired up with the symbols in arsdk.d.ts. 
        any type mismatch would go unnoticed anyway. 
        maybe keep it TS just for documentation purposes 
        but don't fret over types in mirror classes. 
*/

import { AREmptyConstructorTag, arEmptyConstructorTag } from "./ar-empty-constructor"
import * as arsdk from '../arsdk'

export interface Requirements { 
    executeNative(objectId: string, methodName: string, ...params: any[]): any
    replicateScriptToNativeChecked(newObject: any, className: string, ...params: any[]): void
    replicateScriptToNativeUnchecked(newObject: any, className: string, ...params: any[]): void
    classesByName: { [name: string]: any }
}
var requirements_: Requirements

var classesByName: { [name: string]: any } = []

export function init(requirements: Requirements) { 
    requirements_ = requirements
    requirements_.classesByName = []
    for (let className in classesByName) { 
        let class_ = classesByName[className]
        requirements_.classesByName[className] = class_
    }
}

//  --- 

//  test interface

/*
export class MyCodable { 
	x = 0
	y = 0
}
classesByName['MyCodable'] = MyCodable

export class Y  {
    objectId: string
    constructor(i_orObjectId: number | string) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "Y", i_orObjectId) 
    }
	i(): number { return requirements_.executeNative(this.objectId, "i") }
	yfoo(): string { return requirements_.executeNative(this.objectId, "yfoo") }
	set_i(newValue: number) { return requirements_.executeNative(this.objectId, "set_i", newValue) }
	takesCodable(c: MyCodable) { return requirements_.executeNative(this.objectId, "takesCodable", c) }
}
classesByName['Y'] = Y

export class X {
	objectId: string
	static staticFoo(): string { return requirements_.executeNative("arsdk_class_X", "staticFoo") }
    static staticWithParam(p0: number): number { return requirements_.executeNative("arsdk_class_X", "staticWithParam", p0) }
    
    //  todo would be nice to enable this getter / setter syntax
	// get i(): number { return requirements_.executeNative(this.objectId, "get__i") }
	// set i(newValue: number) { requirements_.executeNative(this.objectId, "set__i", newValue) }
	get_i(): number { return requirements_.executeNative(this.objectId, "get__i") }
	set_i(newValue: number) { requirements_.executeNative(this.objectId, "set__i", newValue) }
	get readOnly(): number { return requirements_.executeNative(this.objectId, "get__readOnly") } 

    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "X", maybeObjectId) 
    }
	
    callCallback2() { return requirements_.executeNative(this.objectId, "callCallback2") }
	setCallback2(callback: (x: X) => void) { return requirements_.executeNative(this.objectId, "setCallback2", callback) }
	thrower() { return requirements_.executeNative(this.objectId, "thrower") }
	setCallback(callback: (n: number) => void) { return requirements_.executeNative(this.objectId, "setCallback", callback) }
	returnString(): string { return requirements_.executeNative(this.objectId, "returnString") }
	fp2(p0: number, p1: number): number { return requirements_.executeNative(this.objectId, "fp2", p0, p1) }
	returnInt(): number { return requirements_.executeNative(this.objectId, "returnInt") }
	foo() { return requirements_.executeNative(this.objectId, "foo") }
	callCallback() { return requirements_.executeNative(this.objectId, "callCallback") }
	createY(): Y { return requirements_.executeNative(this.objectId, "createY") }
	takesOptional(p0: number | null) { return requirements_.executeNative(this.objectId, "takesOptional", p0) }
}
classesByName['X'] = X

export class ARScript_Printer {
	objectId: string
	static print(p0: string) { return requirements_.executeNative("arsdk_class_ARScript_Printer", "print", p0) }
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ARScript_Printer", maybeObjectId) 
    }
}
classesByName['ARScript_Printer'] = ARScript_Printer

export function print(str: any) {
	console.log(str.toString())
	ARScript_Printer.print(str.toString())
}

//  globals
let nativeX: X

*/
//  --- ARSDK ---

class ARScript_Printer {
    private objectId: string
	static print(p0: string) { return requirements_.executeNative("arsdk_class_ARScript_Printer", "print", p0) }
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ARScript_Printer", emptyTagOrNoParam) 
    }
}
classesByName["ARScript_Printer"] = ARScript_Printer

class ObjectInstance {
    private objectId: string
	get position() { return requirements_.executeNative(this.objectId, "get__position") }
	set position(newValue) { requirements_.executeNative(this.objectId, "set__position", newValue) }
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ObjectInstance", emptyTagOrNoParam) 
    }    
	setScale(p0: number[]) { return requirements_.executeNative(this.objectId, "setScale", p0) }
	setOpacity(p0: number) { return requirements_.executeNative(this.objectId, "setOpacity", p0) }
	setPosition(p0: number[]) { return requirements_.executeNative(this.objectId, "setPosition", p0) }
    setLocalOrientation(p0: number[]) { return requirements_.executeNative(this.objectId, "setLocalOrientation", p0) }
	whenSelected(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenSelected", p0) }
	whenPlaced(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenPlaced", p0) }
	stopAllAnimations() { return requirements_.executeNative(this.objectId, "stopAllAnimations") }
	whenDeleted(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenDeleted", p0) }
	getPosition(): number[] { return requirements_.executeNative(this.objectId, "getPosition") }
    getLocalOrientation(): number[] { return requirements_.executeNative(this.objectId, "getLocalOrientation") }
	playAnimation(p0: string) { return requirements_.executeNative(this.objectId, "playAnimation", p0) }
    getAnimationNames(): string[] { return requirements_.executeNative(this.objectId, "getAnimationNames") }
}
classesByName["ObjectInstance"] = ObjectInstance

class ScriptExperience {
    private objectId: string
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ScriptExperience", emptyTagOrNoParam) 
    }    
	getObjectInstance(p0: string): ObjectInstance { return requirements_.executeNative(this.objectId, "getObjectInstance", p0) }
    startCreatingInstance(p0: ObjectDefinition) { return requirements_.executeNative(this.objectId, "startCreatingInstance", p0) }
	setInterval(p0: number, p1: () => void) { return requirements_.executeNative(this.objectId, "setInterval", p0, p1) }
	getCameraPosition(): number[] { return requirements_.executeNative(this.objectId, "getCameraPosition") }
    getCameraAngles(): number[] { return requirements_.executeNative(this.objectId, "getCameraAngles") }
    getObjectDefinitionNames(): string[] { return requirements_.executeNative(this.objectId, "getObjectDefinitionNames") }
	getObjectDefinition(p0: string): ObjectDefinition { return requirements_.executeNative(this.objectId, "getObjectDefinition", p0) }
	getPlanePosition(): number[] { return requirements_.executeNative(this.objectId, "getPlanePosition") }
    // whenUpdated(p0: (time: number) => void) { return requirements_.executeNative(this.objectId, "whenUpdated", p0) }
    addObjectScript(objectDefinitionUid: string, onInstanceCreated: (newEntity: ECSEntity) => void) { 
        return requirements_.executeNative(this.objectId, "addObjectScript", objectDefinitionUid, onInstanceCreated)
    }
    getComponentsOfAllEntitiesByTypeNames(typeNames: string[]): ECSEntity[] { 
        return requirements_.executeNative(this.objectId, "getComponentsOfAllEntitiesByTypeNames", typeNames)
    }
    getComponentsOfAllEntities(ctors: AnyComponentClass[]): ECSEntity[] { 
        let typeNames = ctors.map(ctor => { 
            if (ctor.nativeClassName === undefined)
                throw `${ctor}.nativeClass === undefined`
            return ctor.nativeClassName
        })
        return this.getComponentsOfAllEntitiesByTypeNames(typeNames)
    }

}
classesByName["ScriptExperience"] = ScriptExperience

class ObjectDefinition {
    private objectId: string
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ObjectDefinition", emptyTagOrNoParam) 
    }    
	whenInstanceCreated(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenInstanceCreated", p0) }
}
classesByName["ObjectDefinition"] = ObjectDefinition

export function print(str: any) {
    console.log(str.toString())
    ARScript_Printer.print(str.toString())
}


//  --- 

interface AnyComponentClass {
    nativeClassName: string
}
interface ComponentClass<T extends ECSComponent> extends AnyComponentClass { 
    new (...args: any[]): T    
}

export class ECSEntity { 
    protected objectId: string
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ECSEntity", emptyTagOrNoParam) 
    }
    addComponent(component: ECSComponent): void { 
        requirements_.executeNative(this.objectId, "addComponent", component) 
    }

    getComponents<T extends ECSComponent>(componentClass: ComponentClass<T>): T[] { 
        /*
            `componentClass` may be a class that doesn't even exist on the native side. 
            on the native side, script-only subclasses are represented as instances of 
            a native superclass, identified by scriptSubclass.nativeClassName. 
            we ask native for a list of all components that have that native superclass as type, 
            then filter the list down on the script side to instances of the requested 
            script-only subclass. 
        */
        let nativeClassName = componentClass.nativeClassName
        if (nativeClassName === undefined)
            throw `${componentClass}.nativeClassName === undefined`
        const components: ECSComponent[] = this.getComponentsByTypeName(nativeClassName)
        const filteredComponents = components.filter(component => component instanceof componentClass)
        const castComponents = filteredComponents.map(component => (component as T))
        return castComponents
    }

    getComponent<T extends ECSComponent>(componentClass: ComponentClass<T>): T | null { 
        const components = this.getComponents(componentClass)
        if (components.length < 1) 
            return null
        return components[0]
    }

    private getComponentsByTypeName(componentTypeName: string): ECSComponent[] { 
        return requirements_.executeNative(this.objectId, "getComponentsByTypeName", componentTypeName) 
    }
}
classesByName["ECSEntity"] = ECSEntity

export class ECSObjectInstance extends ECSEntity { 
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        super(arEmptyConstructorTag)
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ECSObjectInstance", emptyTagOrNoParam)
    } 
}
classesByName["ECSObjectInstance"] = ECSObjectInstance

export class ECSExperience extends ECSEntity { 
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        super(arEmptyConstructorTag)
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ECSExperience", emptyTagOrNoParam)
    } 
}
classesByName["ECSExperience"] = ECSExperience

export class ECSComponent { 
    static nativeClassName = "ECSComponent"
    protected objectId: string
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ECSComponent", emptyTagOrNoParam) 
    }    
    getEntity(): ECSEntity { return requirements_.executeNative(this.objectId, "getEntity") }
}
classesByName["ECSComponent"] = ECSComponent

export class ECSEmptyComponent extends ECSComponent { 
    static nativeClassName = "ECSBasicEmptyComponent"

    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        super(arEmptyConstructorTag)
        requirements_.replicateScriptToNativeChecked(this, "ECSEmptyComponent", emptyTagOrNoParam) 
    }
}
classesByName["ECSEmptyComponent"] = ECSEmptyComponent

export class ECSTransform extends ECSComponent { 
    //  TODO
    static nativeClassName = "ECSBasicScriptTransformComponent"
    constructor(emptyTag: AREmptyConstructorTag)
    constructor()
    constructor(emptyTagOrNoParam?: AREmptyConstructorTag) { 
        super(arEmptyConstructorTag)
        requirements_.replicateScriptToNativeChecked(this, "ECSTransform", emptyTagOrNoParam) 
    }    

    getParent(): ECSTransform | null { return requirements_.executeNative(this.objectId, "getParent") }
    getChildCount(): number { return requirements_.executeNative(this.objectId, "getChildCount") }
    getChild(index: number): ECSTransform | null { return requirements_.executeNative(this.objectId, "getChild", index) }

    // get position()
	// set position(newValue)
    getWorldPosition(): number[] { return requirements_.executeNative(this.objectId, "getWorldPosition") }
    setWorldPosition(p0: number[]): void { return requirements_.executeNative(this.objectId, "setWorldPosition", p0) }
    getWorldOrientation(): number[] { return requirements_.executeNative(this.objectId, "getWorldOrientation") }
    setWorldOrientation(p0: number[]): void { return requirements_.executeNative(this.objectId, "setWorldOrientation", p0) }
    getLocalScale(): number[] { return requirements_.executeNative(this.objectId, "getLocalScale") }
    setLocalScale(p0: number[]): void { return requirements_.executeNative(this.objectId, "setLocalScale", p0) }
}
classesByName["ECSTransform"] = ECSTransform

export class ECSOnFrame extends ECSComponent { 
    //  TODO native class name should probably be a protocol, not the Basic class? 
    static nativeClassName = "ECSBasicOnFrameComponent"
    constructor(emptyTag: AREmptyConstructorTag)
    constructor(entity: ECSEntity, onFrameAction: arsdk.ECSOnFrame.OnFrameAction)
    constructor(entityOrEmptyTag: ECSEntity | AREmptyConstructorTag, onFrameAction?: arsdk.ECSOnFrame.OnFrameAction) { 
        super(arEmptyConstructorTag)
        requirements_.replicateScriptToNativeChecked(this, "ECSOnFrame", entityOrEmptyTag, onFrameAction) 
    }
    setOnFrameAction(action: () => void): void { return requirements_.executeNative(this.objectId, "setOnFrameAction", action) }
}

export class ECSOnTap extends ECSComponent { 
    //  TODO native class name should probably be a protocol, not the Basic class? 
    static nativeClassName = "ECSBasicOnTapComponent"
    constructor(emptyTag: AREmptyConstructorTag)
    constructor(entity: ECSEntity, onTapAction: arsdk.ECSOnTap.OnTapAction)
    constructor(entityOrEmptyTag: ECSEntity | AREmptyConstructorTag, onTapAction?: arsdk.ECSOnTap.OnTapAction) { 
        super(arEmptyConstructorTag)
        requirements_.replicateScriptToNativeChecked(this, "ECSOnTap", entityOrEmptyTag, onTapAction) 
    }
    setOnTapAction(action: (worldPos: number[]) => void): void { 
        return requirements_.executeNative(this.objectId, "setOnTapAction", action) 
    }
}

//  ---

export let experience: ScriptExperience
export let ecsExperience: ECSEntity
