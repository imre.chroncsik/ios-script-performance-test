
/*
    Implements some scripting-related helpers
    (agnostic of ARSDK, could be reused in other projects for scripting support). 
    Its most important usage is to provide functionality required by 
    other classes / modules; eg. remote-bootstrap uses an ARScript instance 
    to provide requirements for the `RemoteApp` class 
    and the `narrow-intf/arsdk-impl` module. 
    
    The implemented helpers are: 
    *   executeNative(objectId, methodName, params: any[]) => result: any
        Built on top of the executeNative_str(callString) requirement. 
    *   executeCallback(callbackJsonString) => result: string
    *   replicateScriptToNativeUnchecked(newObject: any, className, params: any[])
        Create a native-side counterpart of `newObject`, 
        an instance of the class identified by `className`, 
        using `params` as the constructor parameters. 
        Once done, the two representations (native and script) of the object 
        will be connected by a shared objectId. 
    *   replicateScriptToNativeChecked(newObject: any, className, params: any[])
        Check if `newObject` is an object that was created on the script side 
        (as opposed to being an object that was created on the native side and then 
        replicated to the script side), 
        and if so, then replicate it to the native side (see replicateScriptToNativeUnchecked()). 
    *   replicateNativeToScript(nativeObject: { className, objectId: string }) => newScriptObject: any
        Create the script-side counterpart of an object newly created on the native side. 

    Requirements of ARScript: 
    *   executeNative_str(callString) => ExecuteNativeStrResult | undefined
        Execute `callString` on the native side, 
        return string results: either a `value` or an `error` inside an ExecuteNativeStrResult. 

*/

import { assert } from "console"
import { AREmptyConstructorTag, arEmptyConstructorTag } from "./ar-empty-constructor"

export interface ExecuteNativeStrResult { 
    value?: string
    error?: string
}

export class ARScript {
    executeNative_str?: ((callString: string) => string) = undefined
    globalNamespace: any | undefined = undefined
    classesByName: { [className: string]: any } = {}

    //  TODO should be private
    callbacks: { [callbackId: string]: any } = {}

    executeNative(objectId: string, methodName: string, ...params: any[]): any {
        if (this.executeNative_str === undefined) return 
        while (params.length > 0 && params[params.length - 1] === undefined)
            params.pop()
        params = params.map(p => this.scriptValueToNativeString(p))
       
        var strCall = JSON.stringify({
            "objectId": objectId,
            "methodName": methodName,
            "parameters": params
        })
        
        var resultString: string = this.executeNative_str(strCall)
        var result: ExecuteNativeStrResult = JSON.parse(resultString)
        if (result.error !== undefined) 
            throw result.error
        if (result.value === undefined)
            throw new Error("result.value === undefined")

        const isConstructor = methodName === "new"
        const resultValue = this.nativeValueStringToScriptValue(result.value, isConstructor)
        return resultValue
    }

    executeCallback(callbackJsonString: string): string {
        try { 
            const callbackObject = JSON.parse(callbackJsonString)
            const callback = this.callbacks[callbackObject.callbackId]
            if (callback === undefined)
                throw "callback not found by id: " + callbackObject.callbackId
            const params = callbackObject.params.map(
                (p: any) => this.nativeValueStringToScriptValue(p, false))
            const value = callback(...params)
            return JSON.stringify({ "value": value })
        } catch(x) { 
            //  TODO replace console.log() with something that doesn't break when running on device
            console.log("exception caught in executeCallback(): ")
            console.log(x)
            return JSON.stringify({ "error": x.toString() })
        }
    }

    replicateScriptToNativeChecked(newObject: any, className: string, ...params: any[]) { 
        /*
            if the first param is AREmptyConstructorTag, then we do _not_ need to 
            replicate the new object to the native side. possible reasons: 
            *   the script-side constructor was called as part of the process of 
                replicating an object from native to script. 
            *   the constructor was called from a subclass constructor. 
                the subclass constructor will handle replicating to native
                (and also passing all constructor parameters to the native-side 
                subclass constructor). 
        */
        if (params.length > 0 && params[0] instanceof AREmptyConstructorTag) return
        this.replicateScriptToNativeUnchecked(newObject, className, ...params)
    }

    replicateScriptToNativeUnchecked(newObject: any, className: string, ...params: any[]) { 
        const nativeObject = this.executeNative("arsdk_class_" + className, "new", ...params)
        this.setObjectId(newObject, nativeObject.objectId)
    }

    replicateNativeToScript(
        nativeObject: { className: string, objectId: string }, 
        name: string | undefined = undefined)
        : any { 
        
        const class_ = this.classesByName[nativeObject.className]
        if (class_ === undefined)  
            throw "could not find class by name: " + nativeObject.className
        const newObject = new class_(arEmptyConstructorTag)
        this.setObjectId(newObject, nativeObject.objectId)

        if (name !== undefined && this.globalNamespace !== undefined)
            this.globalNamespace[name] = newObject

        return newObject
    }


    //  private
    private userMainFunction = null
    private objectsById: { [objectId: string]: any } = {}
    private nextCallbackId = 0

    private setObjectId(object: any, objectId: string) { 
        object.objectId = objectId
        this.objectsById[objectId] = object
    }

    private scriptValueToNativeString(scriptValue: any): string {
        return JSON.stringify(this.scriptValueToNativeValue(scriptValue))
    }

    private scriptValueToNativeValue(scriptValue: any): any {
        if (scriptValue === null)
            return "arsdk__nil__"

        if (typeof scriptValue === "function") {
            const callbackId = this.nextCallbackId++
            this.callbacks[callbackId] = scriptValue
            return callbackId
        }

        if ((typeof scriptValue === "object") && ("objectId" in scriptValue)) { 
            return scriptValue.objectId
        }

        if (Array.isArray(scriptValue)) { 
            let scriptValues = scriptValue
            let valueCount = scriptValues.length
            let nativeValueStrings = Array(valueCount)
            for (let v = 0; v < valueCount; ++v) 
                nativeValueStrings[v] = this.scriptValueToNativeValue(scriptValues[v])
            return nativeValueStrings
        }
        
        return scriptValue
    }

    private nativeValueToObjectId(nativeValue: any): string | null { 
        if (typeof nativeValue === "string"
            && nativeValue.indexOf("arsdk__objectId_") !== -1) {
            const objectId = nativeValue.substring("arsdk__objectId_".length)
            return objectId
        }
        return null
    }

    private nativeValueStringToScriptValue(
        nativeValueString: string, isConstructorResult: boolean): any {

        if (nativeValueString === "()")
            return undefined
            
        let maybeObjectId = this.nativeValueToObjectId(nativeValueString)
        if (maybeObjectId !== null)
            return maybeObjectId

        // if (typeof nativeValue === "string"
        //     && nativeValue.indexOf("arsdk__objectId_") !== -1) {
        //     const objectId = nativeValue.substring("arsdk__objectId_".length)
        //     return objectId
        // }
        
        //  if none of the above special cases, then it should be a JSON string. 
        let nativeValue = JSON.parse(nativeValueString)
        return this.nativeValueToScriptValue(nativeValue, isConstructorResult)
    }

    private nativeValueToScriptValue(nativeValue: any, isConstructorResult: boolean): any {
        //  TODO remove the duplication between nativeValueStringToScript() and nativeValueToScript
        if (nativeValue === "()")
            return undefined

        let maybeObjectId = this.nativeValueToObjectId(nativeValue)
        if (maybeObjectId !== null)
            return maybeObjectId

        if (Array.isArray(nativeValue)) { 
            assert(!isConstructorResult)
            let nativeValues = nativeValue
            let valueCount = nativeValues.length
            let scriptValues = Array(nativeValues.length)
            for (let v = 0; v < valueCount; ++v)
                scriptValues[v] = this.nativeValueToScriptValue(nativeValues[v], false)
            return scriptValues
        }

        if (typeof nativeValue === "object"
            && nativeValue.arsdk_objectId !== undefined
            && nativeValue.arsdk_className !== undefined) {
            
            const nativeObject = { 
                className: nativeValue.arsdk_className, 
                objectId: nativeValue.arsdk_objectId }
            if (isConstructorResult) {
                return nativeObject
            } else {
                var object = this.objectsById[nativeObject.objectId]
                if (object === undefined) 
                    object = this.replicateNativeToScript(nativeObject)
                return object
            }
        }
        
        return nativeValue
    }
}

