

//  --- test interface ---

/*

export declare class MyCodable { 
	x: number
	y: number
}

export declare class Y  {
    objectId: string
    constructor(i: number)
    i(): number
    yfoo(): string
    set_i(newValue: number): void
    takesCodable(c: MyCodable): void
}

export declare class X {
    static staticFoo(): string
    static staticWithParam(p0: number): number
    //  todo would be nice to enable this getter / setter syntax
    // get i(): number
    // set i(newValue: number)
    get_i(): number
    set_i(newValue: number): void
    readOnly(): number
    	
    callCallback2(): void
    setCallback2(callback: (x: X) => void): void
    thrower(): void
    setCallback(callback: (n: number) => void): void
    returnString(): string
    fp2(p0: number, p1: number): number
    returnInt(): number
    foo(): void
    callCallback(): void
    createY(): Y
    takesOptional(p0: number | null): void
}

export declare function print(str: any): void 

export let nativeX: X

*/

//  --- ARSDK interface ---

export class ObjectInstance {
	// get position()
	// set position(newValue)
    setScale(p0: number[]): void 
    setOpacity(p0: number): void
    setPosition(p0: number[]): void
    setLocalOrientation(p0: number[]): void
    whenSelected(p0: (instance: ObjectInstance) => void): void
    whenPlaced(p0: (instance: ObjectInstance) => void): void
    stopAllAnimations(): void
    whenDeleted(p0: (instance: ObjectInstance) => void): void
    getPosition(): number[]
    getLocalOrientation(): number[]
    playAnimation(p0: string): void
    getAnimationNames(): string[]
}

export class ScriptExperience {
    getObjectInstance(p0: string): ObjectInstance
    setInterval(p0: number, p1: () => void): void
    getCameraPosition(): number[] 
    getCameraAngles(): number[] 
    getObjectDefinitionNames(): string[]
    getObjectDefinition(p0: string): ObjectDefinition 
    getPlanePosition(): number[]
    startCreatingInstance(p0: ObjectDefinition): void
    // whenUpdated(p0: (time: number) => void): void
    addObjectScript(objectDefinitionUid: string, callback: (newEntity: ECSEntity) => void): void
    getComponentsOfAllEntitiesByTypeNames(typeNames: string[]): ECSEntity[]
    getComponentsOfAllEntities(ctors: AnyComponentClass[]): ECSEntity[]
}

export class ObjectDefinition {
    whenInstanceCreated(p0: (instance: ObjectInstance) => void): void
}

export function print(str: any): void

//  ---

interface AnyComponentClass {}
interface ComponentClass<T extends ECSComponent> extends AnyComponentClass { 
    new (...args: any[]): T    
}

export class ECSEntity { 
    getComponent<T extends ECSComponent>(componentClass: ComponentClass<T>): T
    getComponents<T extends ECSComponent>(componentClass: ComponentClass<T>): T[]
    addComponent(component: ECSComponent): void
}

export class ECSObjectInstance extends ECSEntity { 

}

export class ECSExperience extends ECSEntity { 

}

export class ECSComponent { 
    getEntity(): ECSEntity
}

export class ECSEmptyComponent extends ECSComponent { 
    constructor(ownerEntity: ECSEntity)
}

export class ECSTransform extends ECSComponent { 
    constructor(ownerEntity: ECSEntity)

    getParent(): ECSTransform | null
    getChildCount(): number
    getChild(index: number): ECSTransform | null

	// get position()
	// set position(newValue)
    getWorldPosition(): number[]
    setWorldPosition(p0: number[]): void
    getWorldOrientation(): number[]
    setWorldOrientation(p0: number[]): void
    getLocalScale(): number[]
    setLocalScale(p0: number[]): void
}

export namespace ECSOnFrame { 
    type OnFrameAction = () => void
}
export class ECSOnFrame extends ECSComponent { 
    constructor(ownerEntity: ECSEntity, action?: ECSOnFrame.OnFrameAction)
    setOnFrameAction(onFrameAction: ECSOnFrame.OnFrameAction): void
}

export namespace ECSOnTap { 
    type OnTapAction = (worldPosition: number[]) => void
}
export class ECSOnTap extends ECSComponent { 
    constructor(ownerEntity: ECSEntity, action?: ECSOnTap.OnTapAction)
    setOnTapAction(onTapAction: ECSOnTap.OnTapAction): void
}

//  ---

export var experience: ScriptExperience
export var ecsExperience: ECSEntity