

/*
    this is the runtime counterpart to arsdk.d.ts: 
    *   the `import * as arsdk from 'arsdk'` statement (in main.ts) will ultimately, 
        at runtime, load and execute this file. 
    *   basically its job is to make sure that that import statement in main.ts 
        works as expected. 
    *   in other words, make sure that the symbols declared in arsdk.d.ts 
        are indeed available at runtime. 
    *   the way it does so depends on platform (android / ios / remote). 
    *   it knows what platform it runs on by looking at the global arsdk_platform variable. 

    the way this file gets executed: 
    *   main.ts has an `import * from 'arsdk'` statement. 
    *   the ts -> js compilation translates that into 
        `arsdk = __importStar(require('arsdk'))`. 
    *   when running in gitpod, `require()` is the standard node.js `require()`, 
        will load arsdk.js (this file). 
    *   when running on device, 
        *   we use `parcel` to package all source files 
            into a single .js bundle. 
        *   `parcel` replaces `require()` with one that loads the module 
            not from a separate file, but from a local array inside the bundle. 
        *   this is why we can use `import` in main.ts even on device 
            (and don't need to hack-remove the `import` the way we did earlier). 

    providing implementations for arsdk symbols: 
    *   this depends on platform. 
    *   ultimately we need `require('arsdk')` (this is what the `import` statement 
        will be translated to) to return an array with the correct exported symbols. 
    *   to make that happen, we need to put all the exported symbols into 
        an array called `exports` in this file. 
    *   if we do this, then executing `import * as arsdk from 'arsdk'` will result in 
        `arsdk` containing all arsdk symbols. 
    *   android: 
        *   the on-device runtime exposes all arsdk symbols as globals. 
        *   then it loads and executes the on-device bundle
            (the entry point is device-bootstrap, loads android-bootstrap > main.ts).  
        *   the `import` statement in main.ts loads this module (arsdk.js). 
        *   here we need to copy the symbols from the global namespace into `exports`. 
    *   cloud scripting (gitpod): 
        *   execution starts not with main.ts, but with arsdk/remote/remote-bootstrap.ts. 
        *   remote-bootstrap.ts imports remote-app.ts, which implements the so-called 
            "narrow interface", most importantly the `executeNative_str()` function. 
        *   then it loads main.ts. 
        *   the `import` statement in main.ts loads this module (arsdk.js). 
        *   here, in arsdk.js, we `require('arsdk-impl')` (see narrow-intf/arsdk-impl.ts), 
            which defines (not only declares but also implements) all arsdk symbols 
            in terms of that narrow interface. 
        *   now we have all arsdk symbols inside an object (returned from `require()`), 
            we can copy them into `exports`. 
    *   ios: 
        *   similar to the cloud case. 
        *   not really ios specific, could be used on any platform that 
            exposes the narrow interface. 
        *   the on-device runtime directly exposes only a "narrow interface", 
            most importantly the `executeNative_str()` function. 
        *   then it loads and executes the bundle. the entry point is device-bootstrap.ts, 
            loads ios-bootstrap, that loads main.ts. 
        *   the `import` in main.ts loads arsdk.js (this file). 
        *   just like in the cloud case, here we load arsdk-impl, 
            and then copy arsdk symbols to `exports`. 
    *   note arsdk-impl (implements arsdk symbols on top of the narrow interface)
        is always part of the on-mobile-device bundle, even on android, 
        but on android it's largely ignored (we only use it to get a list of symbol 
        names that we need to look for in the global namespace). 
*/

let platform = global.arsdk_platform

//  re-export arsdk symbols in a platform-agnostic, unified way. 
//  (this practically means adding all arsdk symbols to the `exports` array here.)

let useNarrowIntf = (platform === "remote") || (platform === "ios")

let remote = process.argv.length > 2 && process.argv[2] === "remote"
let arsdkImpl = undefined
if (useNarrowIntf) { 
    arsdkImpl = require('./narrow-intf/arsdk-impl')
} else {
    //  using the wide interface, eg. on android.  
    //  assume that all arsdk symbols are exposed as globals, 
    //  copy them into arsdkImpl so we can later export them in a unified way. 

    //  these are two alternative implementations, 
    //  #b would be preferred but i'm not sure if it would really work. 

    //  a)  use a hardwired list of symbol names to copy symbols from the global namespace to arsdkImpl. 
    // arsdkImpl = {}
    // let symbolNames = ['X', 'Y', 'ARScript_printer', 'print', 'Globals', 'globals']
    // for (let symbolName of symbolNames) 
    //     arsdkImpl[symbolName] = global[symbolName]
    
    //  b)  get a list of symbols that need to be exported
    //      by loading narrow-intf/arsdk-impl even on wide interfaces. 
    //      we only use it to get a list of symbols, but ignore (overwrite) its contents. 
    arsdkImpl = require('./narrow-intf/arsdk-impl')
    for (let symbolName in arsdkImpl)
        arsdkImpl[symbolName] = global[symbolName]
}

//  now we have a platform-independent, unified list of 
//  symbols and their implementations in arsdkImpl; export them. 
//  `import * as arsdk from 'arsdk'` will result in `arsdk` being a ref to the `exports` array here; 
//  so basically this is where we connect the names defined in arsdk.d.ts with their implementations. 
for (let key in arsdkImpl) { 
    if (key.indexOf("__") == 0) continue
    exports[key] = arsdkImpl[key] 
}
