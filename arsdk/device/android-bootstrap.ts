
/*
    On android we simply load and execute the main user script. 
    (On iOS / remote this is more complicated; see arsdk/README.md > Bootstrapping.)
*/

import * as main from '../../src/main'
