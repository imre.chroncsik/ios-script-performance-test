
/*
    Assemble a remote script execution environment, 
    establish a network communication channel between the native and script sides, 
    then import and launch the user script. 
    The native side is running in an ARSDK app on a mobile device, 
    while the script side is running remotely from that device, 
    in a separate JavaScript execution environment, 
    eg. in Gitpod, or on a desktop computer with VSCode. 
    Calls between the two sides will travel over a network channel. 

	Components used to build the environment: 
    *	arscript: provides generic (app-agnostic) native-script bridging services. 
    *	arsdk-impl: provides implementations for ARSDK symbols
        (the symbols declared in arsdk.d.ts). 
    *	remote-app: represents a remote native app (running on a mobile device), 
        implements network communication, translates between ARSDK calls and network messages. 
    *	We wire up these three to provide each other's external requirements, eg.:
        *   remote-app provides executeNative_str() to arscript. 
        *   arscript provides executeNative(), replicateScriptToNative() to arsdk-impl. 
        *   arscript provides executeCallback(), replicateNativeToScript() to remote-app.     
*/

(global as any).arsdk_platform = "remote"

import * as arsdkImpl from "../narrow-intf/arsdk-impl"
import { ARScript } from "../narrow-intf/arscript"
import { RemoteApp } from "./remote-app"

let arscript = new ARScript()
arsdkImpl.init(arscript)

let remoteAppDependencies = { 
	executeCallback: 
		(callbackJsonString: string): string => { 
			return arscript.executeCallback(callbackJsonString) }, 
	replicateNativeToScript: 
		(nativeObject: { className: string, objectId: string }, name: string | undefined) => { 
			return arscript.replicateNativeToScript(nativeObject, name) }}
let remoteApp = new RemoteApp(remoteAppDependencies)
arscript.executeNative_str = (callString) => remoteApp.executeNative_str(callString)
arscript.globalNamespace = arsdkImpl


const mainModuleImportName = '../../src/main'
let modulesToReloadOnRestart: NodeJS.Module[] = []

/*
    we only load main.ts upon receiving a "start" message 
    (as opposed to auto-loading it immediately) 
    for two reasons: 
    1)  the native side may need to do some further initialization 
        after loading the script bundle, but before running the user script, 
        eg. exporting global objects. 
        exporting globals doesn't work without some of the arsdk-script 
        scaffolding being already in place (eg. classes loaded from arsdk-impl), 
        but the user script can't run before globals are exported. 
    2)  so that we can re-start the script by sending "start" again. 
*/
arscript.callbacks["start"] = () => { 
    arsdk_restartScript() 
}

function arsdk_restartScript() {
    for (let module of modulesToReloadOnRestart)
        delete require.cache[module.filename]
    require(mainModuleImportName)
    return undefined
}


//  hijack nodejs's require(), so we can keep track of what modules main.ts loads. 
//  when restarting the script, we'll need to clear these out from the nodejs module cache. 

const mainModuleResolvedFilename = require.resolve(mainModuleImportName)
const originalRequire = module.constructor.prototype.require

module.constructor.prototype.require = function (_request: any): any {    
    let module = originalRequire.apply(this, arguments)

    let parentModule: NodeJS.Module = this
    let mainModule = parentModule.children.find(
        m => m.filename === mainModuleResolvedFilename)
    if (mainModule !== undefined) { 
        let modulesExcludedFromReloadingOnRestart = [
            //  reloading arsdk-impl would null the effects of 
            //  the arsdkImpl.init() in this file above. 
            require.resolve('../narrow-intf/arsdk-impl') ]
        modulesToReloadOnRestart = []
        modulesToReloadOnRestart.push(mainModule)
        for (let i = 0; i < modulesToReloadOnRestart.length; ++i) { 
            let module = modulesToReloadOnRestart[i]
            for (let child of module.children) 
                if (!modulesExcludedFromReloadingOnRestart.includes(child.filename))
                    modulesToReloadOnRestart.push(child)
        }
    }
   
    return module
}


//  watch main.js, restart the script when it changes. 
//  we have tsc running in watch mode, so any changes in main.ts, 
//  or modules imported by main.ts, 
//  results in tsc doing a new main.ts -> main.js compilation. 
//  here we detect that, and restart the script. 

import * as fs from 'fs'

var reloadedAt: number
fs.watch(
    '.ts-out/src/main.js', 
    (_1, _2) => { 
        var stats = fs.statSync('.ts-out/src/main.js')
        //  for some reason this watch handler sometimes gets called 
        //  twice per change, skip one of them. 
        if (reloadedAt == stats.mtime.getSeconds()) 
            return
        reloadedAt = stats.mtime.getSeconds()      
        console.log("--- main.js changed, reloading")
        arsdk_restartScript()
    })
